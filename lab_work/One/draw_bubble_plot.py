import matplotlib
import matplotlib.pyplot as plt
import seaborn as sns
import pickle
matplotlib.use('TkAgg')

# Constants
num_runs = 5
n_values = list(range(1, 2001))

# Theoretical analysis (linear time complexity)
theoretical_complexity = [0.000000021 * i*i for i in range(1, 2001)]


with open('bubble_perf.pkl', 'rb') as pick:
    execution_times = pickle.load(pick)

# Plot the data
sns.set_theme()
sns.lineplot(x=n_values, y=execution_times, label="Empirical Time Complexity", )
sns.lineplot(x=n_values, y=theoretical_complexity, label="Theoretical Time Complexity (O(n*2))", linestyle='--')
plt.xlabel('n')
plt.ylabel('Average Execution Time (s)')
plt.title('Empirical vs. Theoretical Time Complexity of the Bubble sort')
plt.legend()
plt.grid(True)
plt.savefig('5.png')
plt.show()
