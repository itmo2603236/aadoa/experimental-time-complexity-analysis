import random
from time import perf_counter

import matplotlib
matplotlib.use('TkAgg')
import matplotlib.pyplot as plt
import seaborn as sns
from decimal import Decimal

# Constants
num_runs = 5
n_values = list(range(1, 2001))
POINT = 1.5


# Function to measure execution time for constant_function
def measure_time(n, num_runs):
    total_time = 0
    for _ in range(num_runs):
        v = [random.randint(1, 10) for _ in range(n)]  # Generate a random vector
        value = Decimal(0)
        start_time = perf_counter()
        for i, v_value in enumerate(v):
            # print(i)
            value += Decimal(v_value) * Decimal(POINT)**Decimal(i)
        end_time = perf_counter()
        # print(value)
        total_time += end_time - start_time
        return total_time / num_runs # Average execution time


execution_times = []

# Measure execution time for each n
for n in n_values:
    avg_time = measure_time(n, num_runs)
    execution_times.append(avg_time)

# Theoretical analysis (linear time complexity)
theoretical_complexity = [0.0000000005 * i * i for i in range(1, 2001)]


# Plot the data
sns.set_theme()
sns.lineplot(x=n_values, y=execution_times, label="Empirical Time Complexity", )
sns.lineplot(x=n_values, y=theoretical_complexity, label="Theoretical Time Complexity (O(n**2))", linestyle='--')
plt.xlabel('n')
plt.ylabel('Average Execution Time (s)')
plt.title('Empirical vs. Theoretical Time Complexity of calculating a Polynomial function')
plt.legend()
plt.grid(True)
plt.savefig('4_1.png')
plt.show()
