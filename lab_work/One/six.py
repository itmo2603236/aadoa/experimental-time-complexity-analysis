import random
from time import perf_counter
import matplotlib
import matplotlib.pyplot as plt
import seaborn as sns
import numpy as np

matplotlib.use('TkAgg')

# Constants
num_runs = 5
n_values = list(range(1, 2001))


def qsort(arr):
    if len(arr) <= 1:
        return arr
    else:
        return (qsort([x for x in arr[1:] if x < arr[0]])
                + [arr[0]] + qsort([x for x in arr[1:] if x >= arr[0]]))


# Function to measure execution time for constant_function
def measure_time(n, num_runs):
    total_time = 0
    for _ in range(num_runs):
        v = [random.randint(1, 50) for _ in range(n)]  # Generate a random vector
        start_time = perf_counter()
        qsort(v)
        end_time = perf_counter()

        total_time += end_time - start_time
        return total_time / num_runs # Average execution time


execution_times = []

# Measure execution time for each n
for n in n_values:
    avg_time = measure_time(n, num_runs)
    execution_times.append(avg_time)

# Theoretical analysis (linear time complexity)
theoretical_complexity = [0.000000050 * i * np.log2(i) for i in range(1, 2001)]


# Plot the data
sns.set_theme()
sns.lineplot(x=n_values, y=execution_times, label="Empirical Time Complexity", )
sns.lineplot(x=n_values, y=theoretical_complexity, label="Theoretical Time Complexity (O(n*log(n)))", linestyle='--')
plt.xlabel('n')
plt.ylabel('Average Execution Time (s)')
plt.title('Empirical vs. Theoretical Time Complexity for the \nQuick sort algorithm')
plt.legend()
plt.grid(True)
plt.savefig('6.png', bbox_inches='tight')

plt.show()
